Feature: Report page
	User can report that zoo's information is outdated or wrong.
	Admin can then change details according to the report.

	Background:
		Given user went into zoo's details page
		And user clicked on Report button

	Scenario: Reporting incorrect information
		When user selects which information is incorrect
		And user enters correct information
		And clicks Submit button
		Then user is given a message "Your report is submitted"
		And user is redirected to Map tab

	Scenario: Back to the details
		When user clicks on Back button
		Then user is redirected to zoo's Details page 