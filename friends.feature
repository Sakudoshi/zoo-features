Feature: Friends tab
	Users can view list of friends profiles
	and add other users to friend list

	Background:
		Given user is logged in
		And user is in Friends tab

	Scenario: Viewing friends
		Then user can view friends

	Scenario: Viewing friend's detailed profile
		When user clicks on friend's profile
		Then user is redirected to friend's profile page

	Scenario: Filtering friends
		When user clicks on search bar
		And user enters a name
		Then user's friend is displayed

	Scenario: Adding a friend
		When user clicks on Add friend button
		And user enters a name
		And user clicks Add me next to user's name
		Then user sees message "Invite sent"

	Scenario: Invite accepted
		Given user sent previously invitation
		When user's invitation to friends was accepted
		Then new friend will be displayed on the list

	Scenario: Getting invite
		Given user received and invitation from another user
		When user sees a prompt to add new friend
		And clicks on Accept
		Then other user is now a friend

	Scenario: Changing to Filter tab
		When user clicks on Filter tab
		Then user is redirected to Filter page

	Scenario: Changing to Map tab
		When user clicks on Map tab
		Then user is redirected to Map page

		