Feature: Remember my password functionality
	User can recover their password using their address email.
	They can only recover password if given email is correct

	Scenario: User submits email to recover password
		Given user entered email address
		When user clicks Submit button
		Then user sees message "Your password reset link was sent to your email"