Feature: Edit profile
	User can change their name, password or about me summary and more

	Background:
		Given user is logged in
		And user is in Edit profile page

	Scenario: Changing name
		Given user clicked Change name button
		When user enters first name
		And user enters last name
		And user clicks Submit
		Then user's name is changed
		And user is redirected to My profile page

	Scenario: Changing password
		Given user clicked Change password button
		When user enters current password
		And user enters new password
		And user enters new password confirmation
		And user clicks Submit
		Then user's password is changed
		And user is redirected to My profile page

	Scenario: Changing email
		Given user clicked Change email button
		When user enters email
		And user clicks Submit
		Then user's email is changed
		And user is redirected to My profile page

	Scenario: Changing avatar
		Given user clicked Change avatar button
		When user enters avatar
		And user clicks Submit
		Then user's avatar is changed
		And user is redirected to My profile page

	Scenario: Changing about me
		Given user clicked Change about me button
		When user enters text
		And user clicks Submit
		Then user's about me is changed
		And user is redirected to My profile page

	Scenario: Back to the my profile
		When user clicks on Back button
		Then user is redirected to My profile page 