Feature: Register
	User must enter correct email address,
	password must be from 8 letters and have one number,
	and other credentials must be inputted correctly.

	Scenario: User creates account
		Given user doesn't yet exist
		When user enters email address
		And user enters password
		And user enters password confirmation
		And user enters first name
		And user enters surname
		And user enters date of birth
		And user clicks on Submit button
		Then user is redirected to Map tab