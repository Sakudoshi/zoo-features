Feature: My profile	
	User can access their own profile
	
	Scenario: Viewing user's own profile
		Given user is logged in
		And user clicked My profile in Map tab
		Then user sees their profile information

	Scenario: Back to the map
		When user clicks on Back button
		Then user is redirected to Map tab 