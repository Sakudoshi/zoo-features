Feature: Viewing map options
	Users can view zoos location on map and details about them.
	Map consists of zoo only from Europe.

	Background: 
		Given user is in the Map tab
		And user has GPS turned on

	Scenario: Viewing map
		Then user can view zoos in a map
		And interact with a map

	Scenario: Viewing info of a zoo
		When user clicks on any zoo
		Then user gets short info of a zoo

	Scenario: Viewing details of a zoo
		Given user clicked on a zoo
		When user clicks on details button
		Then user is redirected to Details page

	Scenario: Changing to Filter tab
		When user clicks on Filter tab
		Then user is redirected to Filter page

	Scenario: Changing to Friends tab
		When user clicks on Friends tab
		Then user is redirected to Friends page




