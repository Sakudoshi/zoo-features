Feature: Viewing map options as admin
	Admin can access in Map tab their Admin Dashboard

	Scenario: Accessing admin dashboard
		Given user is logged in as admin
		And admin is in the Map tab
		When admin clicks on their avatar
		And admin clicks on Admin Dashboard button
		Then user is redirected to Admin Dashboard page