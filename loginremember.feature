Feature: Remembering a previous login
	Users are automatically logged in if they did it once,
	unless they clear cache

	Scenario: Logging in automatically
		Given user logged in previously
		And user closed an app
		And has access to the Internet
		When user opens an app
		Then user is redirected to Map tab