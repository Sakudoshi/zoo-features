Feature: Admin dashboard
	From here admin can change info about zoos, delete users and view reports

	Background:
		Given user is logged in as admin
		And admin is in admin dashboard page

	Scenario: Viewing options
		Then admin sees options to moderate app

	Scenario: Search zoo
		When admin clicks on search zoo 
		And enters a name
		And admin clicks on zoo
		Then admin can view zoo's details

	Scenario: Search user
		When admin clicks on search user bar
		And admin enters a name
		Then admin can see a user's name

	Scenario: Delete user
		Given admin searched for user
		And admin sees his name
		When admin clicks Delete user button
		And admin clicks Ok on prompt
		Then admin gets message "User is deleted"

	Scenario: View reports
		When admin clicks view reports button
		Then admin is redirected to Reports page

