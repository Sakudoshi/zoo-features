Feature: Main menu
	Not logged user can choose here to login or to register
	App's logo is also visible
	
	Background:
		Given user opens app
		And is not already logged in
		And has access to the Internet

	Scenario: Viewing menu
		Then user sees main menu of the app

	Scenario: Accessing login page
		When user clicks on Login button
		Then user is redirected to Login page

	Scenario: Accessing register page
		When user clicks on Register button
		Then user is redirected to Register page