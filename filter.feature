Feature: Filter tab
	Users can filter their zoos to view only those, that accept specific criteria

	Background:
		Given user is in Filter tab

	Scenario: Filtering animals
		When user chooses by which animals to search for
		Then user is redirected to map tab
		And only zoos with filtered animals are displayed

	Scenario: Filtering rating
		When user chooses by which rating to search for
		Then user is redirected to map tab
		And only zoos with filtered rating are displayed

	Scenario: Filtering visited
		When user chooses to search for visited zoos
		Then user is redirected to map tab
		And only visited zoos are displayed

	Scenario: Filtering unvisited
		When user chooses to search for unvisited zoos
		Then user is redirected to map tab
		And only unvisited zoos are displayed

	Scenario: Changing to Map tab
		When user clicks on Map tab
		Then user is redirected to Map page

	Scenario: Changing to Friends tab
		When user clicks on Friends tab
		Then user is redirected to Friends page
