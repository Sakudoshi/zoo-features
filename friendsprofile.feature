Feature: Friend's profile
	User can see their friend's details,
	what they visited and rate and unfriend them

	Background:
		Given user clicked on friend's profile

	Scenario: Viewing friend's details
		Then user sees friend's details

	Scenario: Unfriend someone
		When user clicks on Unfriend button
		And user clicks Ok on prompt
		Then user gets message "You're not friends anymore"
		And user is redirected to Friends tab

