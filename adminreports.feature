Feature: Admin reports
	Admin can view reports that users created and delete them

	Background:
		Given admin is logged in
		And admin is in Reports page

	Scenario: View report list
		Then admin sees list of reports

	Scenario: View a report
		When admin clicks on a report from list
		Then admin sees report's details

	Scenario: Delete a report
		When admin clicks on Delete report on a report from list
		Then report is deleted

	Scenario: Back to dashboard
		When admin clicks on Back button
		Then admin is redirected to Admin Dashboard
