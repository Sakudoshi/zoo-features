Feature: Changing forgotten password
	User can change their password after clicking on a link from email

	Scenario: User changes forgotten password
		Given user opens change password page from email link
		And user has access to the Internet
		When user enters password
		And user enters confirmation password
		Then user sees message "Password successfully reseted"
		And user is redirected to Login page