Feature: Admin Zoo details
	Admin specific settings in zoo's details page.
	They can edit a zoo and lock it from being updated

	Background: 
		Given admin is logged in
		And admin is in zoo's details page

	Scenario: Edit zoo
		When admin clicks Edit button
		And admin edits any information in zoo's details
		And admin clicks Submit button
		Then admin sees message "Zoo's edited"

	Scenario: Lock zoo
		When admin clicks Lock button
		Then admin sees message "Zoo locked from updating"

	Scenario: Unlock zoo
		And admin locked zoo
		When admin clicks Unlock button
		Then admin sees message "Zoo can be updated"
