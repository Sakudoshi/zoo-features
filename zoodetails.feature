Feauture: Zoo details
	Users can see more information about a zoo.
	Where it is, what animals are there,
	who visited it. They can also change their rating	

	Background:
		Given user clicked on any zoo on the map
		And user clicked Details button

	Scenario: Viewing details
		Then user sees details about zoo
		And user sees friends that visited a zoo

	Scenario: Viewing details of a friend that visited a zoo
		Given a friend of the user visited a zoo
		When user clicks on friend's profile
		Then user is redirected to friend's profile page

	Scenario: Reporting incorrect information
		When user clicks on Report outdated info
		Then user is redirected to Report page

	Scenario: Changing the rating
		Given user visited a zoo
		And user gave zoo a rating
		When user clicks on different star rating
		Then user changed a rating

	Scenario: Back to the map
		When user clicks on Back button
		Then user is redirected to Map tab 