Feature: Login functionality
	Users can login using their emails or Google account.
	Users can also try to recover their password.
	There will be error if user enters incorrect email or password.
	There will be error if user doesn't exist in database

	Scenario: User logging in using email
		Given user entered email address
		And entered password
		When user clicks Submit button
		Then user is redirected to Map tab

	Scenario: User logging in Google Account
		Given user clicked Google Login button
		And entered correctly credentials
		When user submits credentials
		Then user is redirected to Map tab

	Scenario: User clicks on Remember my password
		When user clicks "Remember my password" button
		Then user is redirected to Remember my password tab

	Scenario: User clicks on Log in as a guest
		When user clicks on Log in as a guest
		Then user is redirected to the Map tab