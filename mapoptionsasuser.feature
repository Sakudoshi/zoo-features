Feature: Viewing map options as registered user
	Registered users can review a zoo and access their profile

	Background:
		Given user is logged in
		And user is in the Map tab
		And user has GPS on
	
	Scenario: Marking a zoo as visited
		Given user sees info of a zoo
		When user clicks on Visited button
		Then user marks a zoo as visited
		And user gets prompt to review a zoo

	Scenario: Reviewing a zoo
		Given user visited a zoo
		When user chooses star rating
		And clicks Submit button
		Then user sees their rating on zoo info screen

	Scenario: Skip reviewing a zoo
		Given user has prompt to rate a zoo
		When user clicks Skip button
		Then user gets back to short info of a zoo

	Scenario: Viewing user's profile
		When user clicks on their avatar
		And user clicks on My Profile button
		Then user is redirected to My profile page

	Scenario: Accessing settings
		When user clicks on their avatar
		And user clicks on Settings button
		Then user is redirected to Settings page

	Scenario: Logging out
		When user clicks on their avatar
		And user clicks on Log out button
		Then user is redirected to Login page
